# Atenea Updates Notifications

Very simple Python script to get notified when new documents are uploaded to Atenea (an instance of Moodle). Makes use of a Gotify server.

## License

The program is licensed under the GPL v3. License is available [here](https://gitlab.com/oscarbenedito/aun/blob/master/COPYING).

## Authors

Alphabetically by last name.

 - **Oscar Benedito** - oscar@obenedito.org
 - **Ernesto Lanchares** - e.lancha98@gmail.com
 - **Ferran López** - flg@tuta.io
